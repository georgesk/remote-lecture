/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var slideNum,
    firstFetchSlide,
    currFetchSlide,
    numberOfSlides;

function slideFromNum(num) {
    return "slides/slide_" + num + ".svg";
};

function displaySlide() {
    if (currFetchSlide !== firstFetchSlide){
        document.getElementById("prefetch").removeEventListener("load",
                fetchNextSlide);
    };
    document.getElementById("slide").src = slideFromNum(slideNum);
};

function fetchNextSlide() {
    currFetchSlide = (currFetchSlide % numberOfSlides) + 1;
    if (currFetchSlide === firstFetchSlide){
        document.getElementById("slide").removeEventListener("load",
            fetchNextSlide);
        return;
    }
    document.getElementById("prefetch").addEventListener("load",
            fetchNextSlide, { once: true });
    document.getElementById("prefetch").src = slideFromNum(currFetchSlide);
};

var socket = io({ auth: { token: "@lecturer-key" } });

var firstSlide = socket.on("firstSlide", function(data) {
    slideNum = data.num;
    numberOfSlides = data.max;
    firstFetchSlide = slideNum;
    currFetchSlide = slideNum;
    document.getElementById("slide").addEventListener("load", fetchNextSlide);
    displaySlide();
});

var goToSlide = socket.on("goToSlide", function(num) {
    slideNum = num;
    displaySlide();
});

/* lecturer start */
document.addEventListener("keydown", function(e){
    switch (e.keyCode) {
    case 37: // left
        socket.emit("incrSlide", -1);
        break;
    case 40: // down
        socket.emit("incrSlide", 10);
        break;
    case 38: // up
        socket.emit("incrSlide", -10);
        break;
    case 39: // right
        socket.emit("incrSlide", 1);
        break;
    case 33: // page up
        socket.emit("prevSlide");
        break;
    case 34: // page up
        socket.emit("lastSlide");
        break;
    }
}, false);
/* lecturer end */
