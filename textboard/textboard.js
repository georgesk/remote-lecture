/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var socket = io({ auth: { token: "@lecturer-key" } });

var updateTextboard0 = socket.on("updateTextboard0", function(data) {
    textboard0 = document.getElementById("textboard0");
    textboard0.innerHTML = data;
    Prism.highlightAllUnder(textboard0);
    jqMath.parseMath(textboard0);
});

var updateTextboard1 = socket.on("updateTextboard1", function(data) {
    textboard1 = document.getElementById("textboard1");
    textboard1.innerHTML = data;
    Prism.highlightAllUnder(textboard1);
    jqMath.parseMath(textboard1);
});
