/**
 * textboard theme for C language and pseudocode
 * for use with Remote Lecture software
 * @author Hugo Raguet
 */

code[class*="language-"],
pre[class*="language-"] {
	font-family: monospace;
	text-align: left;
	white-space: pre;
	word-spacing: normal;
	word-break: normal;
	word-wrap: normal;

	-moz-tab-size: 4;
	-o-tab-size: 4;
	tab-size: 4;

	-webkit-hyphens: none;
	-moz-hyphens: none;
	-ms-hyphens: none;
	hyphens: none;
}

/* Code blocks */
pre[class*="language-"] {
	overflow: auto;
}

/* Inline code */
:not(pre) > code[class*="language-"] {
	padding: .1em;
	white-space: normal;
}

/* vim basic dark theme */

.token.comment {
	color: #00ffff;
}

.token.char,
.token.number,
.token.string,
.token.constant {
	color: #ff40ff;
}

.token.punctuation,
.token.statement,
.token.operator {
	color: #ffff00;
}

.token.directive,
.token.directive-hash {
	color: #5fd7ff;
}

.token.type {
    color: #87ffaf; 
}

.token.function {
    color: #00ffff;
    font-weight: bold;
}

/* pseudocode */
.language-pseudo .token.comment {
	color: #d7d7d7;
    font-style: italic;
}

.language-pseudo .token.boolean,
.language-pseudo .token.string,
.language-pseudo .token.char,
.language-pseudo .token.number,
.language-pseudo .token.empty,
.language-pseudo .token.constant {
    color: #ffd7ff;
}

.language-pseudo .token.keyword {
    font-weight: bold;
}

.language-pseudo .token.operator {
    color: #ffffd7;
}

.language-pseudo .token.function {
    color: #d7ffff;
    font-weight: normal;
}
