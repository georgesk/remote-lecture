/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const BoardData = require("./boardData.js").BoardData;
const config = require('./configuration.js');
const fs = require('fs');
const path = require('path');

/** Map from name to *promises* of BoardData
	@type {Object<string, Promise<BoardData>>}
*/
var boards = {};

function getBoard(name) {
  if (!boards.hasOwnProperty(name)) {
    boards[name] = new BoardData(name);
  }
  return boards[name];
}

function handleMessage(boardName, message, socket) {
    if (message.tool === "Cursor"){
        message.socket = socket.id;
    }else if (message.tool === "clean"){
        delete boards[boardName];
    }else if (message.tool === "clean-all"){
        for (var key in boards){
            if (key.match(/slide_(\d+)/)){ delete boards[key]; }
        }
    }else if (message.tool !== "Pointer"){
        saveHistory(boardName, message);
    }
}

async function saveHistory(boardName, message) {
  var id = message.id;
  var board = await getBoard(boardName);
  switch (message.type) {
    case "delete":
      if (id) board.delete(id);
      break;
    case "update":
      if (id) board.update(id, message);
      break;
    case "child":
    case "endline":
      board.addChild(message.parent, message);
      break;
    default:
      //Add data
      if (!id) throw new Error("Invalid message: ", message);
      board.set(id, message);
  }
}

/* export client-side configuration */
fs.writeFile("drawboard/configuration.js",
    "const server_config = " + JSON.stringify({
        MAX_EMIT_COUNT: config.drawboard.MAX_EMIT_COUNT,
        MAX_EMIT_COUNT_PERIOD: config.drawboard.MAX_EMIT_COUNT_PERIOD,
        AUTO_FINGER_WHITEOUT: config.drawboard.AUTO_FINGER_WHITEOUT,
        BASE_RESOURCE_PATH: config.drawboard.BASE_RESOURCE_PATH,
        POINTER_TIMEOUT_MS: config.drawboard.POINTER_TIMEOUT_MS,
        CURSOR_DELETE_AFTER_MS: config.drawboard.CURSOR_DELETE_AFTER_MS,
        SVG_VIRTUAL_WIDTH: config.drawboard.SVG_VIRTUAL_WIDTH,
        MIN_TOOL_SIZE: config.drawboard.MIN_TOOL_SIZE,
        MAX_TOOL_SIZE: config.drawboard.MAX_TOOL_SIZE
    }) + ";",
    function (err) { if (err) console.log(err); }
);

module.exports = { handleMessage, getBoard };
