/***  general parameters  ***/

const PORT = 30000;

let now = new Date();
const TITLE = "Cours à distance"
    + " — "
    + String(now.getDate()).padStart(2, "0") + "/"
    + String(now.getMonth() + 1).padStart(2, "0") + "/"
    + now.getFullYear();

const LECTURER_KEY = "lecturer";
    // String(Math.floor(Math.random() * (10**10))).padStart(10, "0");

const CONNECTION_STATS_FILE = "server/connection_stats.log";

const beamer = {
    CACHE_MAX_AGE: 4800 // 1h 20 min
};

const drawboard = {
    MAX_EMIT_COUNT: 192,
    MAX_EMIT_COUNT_PERIOD: 4096,
    AUTO_FINGER_WHITEOUT: true,
    BASE_RESOURCE_PATH: "../drawboard/",
    POINTER_TIMEOUT_MS: 1000 * 2,
    CURSOR_DELETE_AFTER_MS: 1000 * 3,
    SVG_VIRTUAL_WIDTH: 10000, // beats any reasonable resolution
    MIN_TOOL_SIZE: 10, // relative to virtual width
    MAX_TOOL_SIZE: 500 // relative to virtual width
};

const terminal = {
    PIPE: "terminal/pipe",
    COLUMNS: 159, // two 79 char buffers side-by-side
    LINES: 44
}

module.exports = {
    PORT, TITLE, LECTURER_KEY, CONNECTION_STATS_FILE,
    beamer, drawboard, terminal
};
