/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const fs = require('fs');
const path = require('path');
const drawboard = require('./drawboard.js');
const config = require('./configuration.js');
const SLIDES_DIR = "beamer/slides";

/* default empty slide */
function emptySlide(){
    return '<svg width="4" height="3" viewBox="0 0 4 3" ' + 
        'xmlns="http://www.w3.org/2000/svg" ' + 
        'xmlns:xlink="http://www.w3.org/1999/xlink">' +
        '<rect width="4" height="3" style="fill:white;stroke-width:0"/>' +
    '</svg>';
}

var localSlideRegExp = new RegExp("slide_(\\d+)\\.svg");

/* counting beamer slides */
var prevSlideNum = 1,
    slideNum = 1,
    numberOfSlides = fs.readdirSync(SLIDES_DIR).filter(fname => fname.match(localSlideRegExp)).length;


function incrSlide(n) {
    prevSlideNum = slideNum;
    slideNum = slideNum + n;
    if (slideNum < 1) slideNum = 1;
    else if (slideNum > numberOfSlides) slideNum = numberOfSlides;
}

function lastSlide(){
    prevSlideNum = slideNum;
    slideNum = numberOfSlides;
}

function prevSlide(){
    slideNum = prevSlideNum;
}

/* add empty slide at the end */
function pushEmptySlide(){
    slides.push(emptySlide());
    numberOfSlides += 1;
}

/* preload beamer slides in memory */
var slides = [];
if (numberOfSlides === 0){
    pushEmptySlide()
}else{
    for (let num = 1; num <= numberOfSlides; num++){
        var slide = path.join(SLIDES_DIR, "slide_" + num + ".svg");
        slides.push(fs.readFileSync(slide, "UTF-8"));
    }
}

/* serve beamer slides; return true if resource is found, false otherwise */
var slideRegExp = new RegExp(path.join(SLIDES_DIR, "slide_(\\d+)\\.svg"));
function serve(url, response) {
    var match = url.match(slideRegExp);
    if (match){
        response.writeHead(200, {
            "Content-Type": "image/svg+xml",
            "Cache-control": "max-age=" + config.beamer.CACHE_MAX_AGE 
                + ", must-revalidate"
        });
        response.end(slides[Number(match[1]) - 1]);
        return true;
    }else{
        return false;
    }
}

/***  communications throught sockets  ***/ 

/* broadcasting slide number */
function goToSlide(socket) {
    socket.emit("goToSlide", slideNum);
    socket.broadcast.emit("goToSlide", slideNum);
}

function onConnection(socket) {
    if (socket.handshake.auth.token === config.LECTURER_KEY){
        socket.on("incrSlide", function(n){
            incrSlide(n);
            goToSlide(socket);
        });
        socket.on("lastSlide", function(){
            lastSlide();
            goToSlide(socket);
        });
        socket.on("prevSlide", function(){
            prevSlide();
            goToSlide(socket);
        });
        socket.on("addEmptySlide", function(){
            pushEmptySlide()
            lastSlide();
            goToSlide(socket);
        });
        socket.on("cleanSlide", function(boardName){
            drawboard.handleMessage(boardName, { tool: "clean" });
            goToSlide(socket);
        });
        socket.on("cleanAllSlides", function(boardName){
            drawboard.handleMessage(boardName, { tool: "clean-all" });
            goToSlide(socket);
        });
        /* broadcasting drawboard messages */
        socket.on("broadcastDraw", function (message) {
            var boardName = message.board;
            var data = message.data;
            drawboard.handleMessage(boardName, data, socket);
            socket.broadcast.emit("broadcastDraw", data);
        });
    }
    
    socket.on("getDrawboard", async function (name) {
        var board = await drawboard.getBoard(name);
        socket.emit("broadcastDraw", { _children: board.getAll() });
    });

    /* start beamer with current slide */
    socket.emit("firstSlide", {
        num: slideNum,
        max: numberOfSlides
    });
}

const info = "Beamer slides are in\n  " +
    path.join(path.resolve("."), SLIDES_DIR);

module.exports = { serve, onConnection, info };
